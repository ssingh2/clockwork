package com.infinityfw.pm;

import java.net.MalformedURLException;

import org.mantisbt.connect.MCException;

public class PMFactory {

	private static final String REDMINE = "REDMINE";
	private static final String JIRA = "JIRA";
	private static final String BUGZILLA = "BUGZILLA";
	private static final String MANTIS="MANTIS";
	private static final String DEFAULT = "DEFAULT";
	private static PMHandler m_PMH = null;
	private static String m_pmToolName = "";

	public static PMHandler getInstance(String pmToolName)
	{
		if ((m_PMH == null) || (!pmToolName.equalsIgnoreCase(m_pmToolName))) {
			m_PMH = newInstance(pmToolName);
		}

		return m_PMH;
	}

	private static PMHandler newInstance(String toolName)
	{
		PMHandler pih = null;

		if (toolName != null) {
			if (toolName.equalsIgnoreCase(REDMINE))
				pih = new RedmineHandler();
			else if (toolName.equalsIgnoreCase(JIRA))
				pih = new JiraHandler();
			else if (toolName.equalsIgnoreCase(BUGZILLA))
				pih = new BugzillaHandler();
			else if (toolName.equalsIgnoreCase(MANTIS))
				try {
					pih = new MantisHandler();
				} catch (MalformedURLException | MCException e) {
					e.printStackTrace();
				}
			else if (toolName.equalsIgnoreCase(DEFAULT))
				pih = new PMHandler();
			else {
				pih = new NoPMHandler();
			}
		}
		return pih;
	}

}
