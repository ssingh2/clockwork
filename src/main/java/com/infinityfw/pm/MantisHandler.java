package com.infinityfw.pm;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.mantisbt.connect.IMCSession;
import org.mantisbt.connect.MCException;
import org.mantisbt.connect.axis.MCSession;
import org.mantisbt.connect.model.IProject;
import org.mantisbt.connect.model.Issue;
import org.mantisbt.connect.model.MCAttribute;

import com.infinityfw.libraries.DynamicQuery;


public class MantisHandler extends PMHandler {


	private static MantisHandler instance = null;
	private static IMCSession session = null;

	public static MantisHandler getInstance() {
		if (instance == null) {
			try {
				instance = new MantisHandler();
			} catch (MalformedURLException ex) {
				Logger.getLogger(MantisHandler.class.getName()).log(Level.SEVERE, null, ex);
			} catch (MCException ex) {
				Logger.getLogger(MantisHandler.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return instance;
	}

	public MantisHandler() throws MalformedURLException, MCException {

		URL url = new URL(DynamicQuery.CONFIG_MANTIS_URL);
		session = new MCSession(url, DynamicQuery.CONFIG_MANTIS_USER,DynamicQuery.CONFIG_MANTIS_PWD);

	}

	public static IMCSession getSession() throws MalformedURLException, MCException {
		if (session == null) {
			getInstance();
		}
		return session;
	}	

	public static String reporIssue(String summary, String desc, String category, String informAdi, String evidence, String nameArchive) {

		IMCSession sessions = null;
		String archive = nameArchive + ".png";
		String bugID= null;
		try {	
			sessions = MantisHandler.getSession();
			IProject projet = sessions.getProject(DynamicQuery.CONFIG_PROJECT);
			Issue issue = new Issue();
			issue.setProject(new MCAttribute(projet.getId(), projet.getName()));
			issue.setAdditionalInformation(null);
			issue.setOs(System.getProperty("os.name"));
			issue.setOsBuild(System.getProperty("os.version"));
			issue.setPlatform(System.getProperty("os.arch"));
			issue.setSeverity(new MCAttribute(70, "crash"));
			issue.setReproducibility(new MCAttribute(10, "always"));
			issue.setSummary(summary + new Date());
			issue.setDescription(desc);
			issue.setCategory(category);
			issue.setPriority(new MCAttribute(40, "high"));
			issue.setAdditionalInformation(informAdi);
			long id = sessions.addIssue(issue);     
			sessions.addIssueAttachment(id, archive, "image/png", Base64.decodeBase64(evidence));
			bugID = String.valueOf (id); 
		} catch (MalformedURLException e) {
			System.err.println("Error no URL access to Mantis");
			e.printStackTrace();
		} catch (MCException e) {
			System.err.println("Error no connection to mantis");
			e.printStackTrace();
		}
		return bugID;
	}


}
