package com.infinityfw.testcases.YahooTest;

import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.infinityfw.driver.Controller;
import com.infinityfw.libraries.BootTestNG;
import com.infinityfw.libraries.ConfigFileReader;
import com.infinityfw.libraries.PublicVariables;
import com.infinityfw.readlxls.ReadingDataSheetOfTestCase;
import com.infinityfw.utillib.ReportUtil;
import com.infinityfw.wrappers.WebObject;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;


public class TC1 {
	public String testName = "TC1";
	public String currentTestSuite = "YahooTest";
	public String testcaseDesc = "LoginTest";
	public String testlinkTCId = "PP-15";
	private ExtentReports extent = ReportUtil.getInstance();


	@BeforeSuite
	public void setUp() {
		ConfigFileReader.loadProperties(System.getProperty("user.dir")+"\\config\\config.properties", false);			
	}
	@Test(dataProvider="getData")
	public void doTC1(Hashtable<String,String> data) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, Exception{
		
		WebObject.REPORT = extent.startTest(testName); 
		if(!Controller.getRunModeOfTestCase(testName, currentTestSuite , BootTestNG.currentTestSuiteXL.get(currentTestSuite))){
			WebObject.REPORT.log(LogStatus.SKIP,"Skipping the Test Case as RUNMODE in TestCases sheet is N");
			throw new SkipException("Skipping the Test Case as RUNMODE in TestCases sheet is N");
		}
		if(data==null){
			WebObject.REPORT.log(LogStatus.SKIP,"Skipping the Test Case as data sheet has no data for this testcase");
			throw new SkipException("Skipping the Test Case as data sheet has no data for this testcase");
		}	
		else{
			if(data.get(PublicVariables.TEST_SUITE_DATA_COLUMN_RUNMODE).equals("N")){
				WebObject.REPORT.log(LogStatus.SKIP,"Skipping the testset as data sheet has RUNMODE=N");
				throw new SkipException("Skipping the testset as data sheet has RUNMODE=N");
			}
			Controller.executeTestStepsSerially(testName,testcaseDesc,testlinkTCId,currentTestSuite, BootTestNG.currentTestSuiteXL.get(currentTestSuite), data);			
		}
	}

	@DataProvider
	public Object[][] getData(){
		return ReadingDataSheetOfTestCase.getData(testName,currentTestSuite, BootTestNG.currentTestSuiteXL.get(currentTestSuite));
	}


	@AfterMethod
	public void tearDown() {
		extent.endTest(WebObject.REPORT);
		extent.flush();

	}

}



