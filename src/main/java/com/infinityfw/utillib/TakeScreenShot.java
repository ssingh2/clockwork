package com.infinityfw.utillib;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.infinityfw.driver.Controller;
import com.infinityfw.libraries.DynamicQuery;
import com.infinityfw.libraries.PublicVariables;
import com.infinityfw.wrappers.SetBaseState;

public class TakeScreenShot{

	public static String mailscreenshotpath;
	private static final Logger logger = Logger.getLogger(Controller.class);
	
	public static void captureScreenshot(String methodName) {
		
		 Calendar cal = new GregorianCalendar();
		  int month = cal.get(Calendar.MONTH);
		  int year = cal.get(Calendar.YEAR);
		  int sec =cal.get(Calendar.SECOND);
		  int min =cal.get(Calendar.MINUTE);
		  int date = cal.get(Calendar.DATE);
		  int day =cal.get(Calendar.HOUR_OF_DAY);
		
		
	
		File srcFile = ((TakesScreenshot)SetBaseState.driver).getScreenshotAs(OutputType.FILE);
	    try {
	    	String[] split = methodName.split("\\(");
	    	mailscreenshotpath = System.getProperty("user.dir")+"\\screenshots\\"+year+"_"+date+"_"+(month+1)+"_"+day+"_"+min+"_" +sec+"_"+split[0]+".jpeg";
	    	logger.info("Path is "+mailscreenshotpath);
			FileUtils.copyFile(srcFile, new File(mailscreenshotpath));
		} catch (IOException e) {
			logger.info("Path is "+mailscreenshotpath);
			e.printStackTrace();
		}
	}

	
	public void takeScreenShot(String fileName,
			String currentTestSuite, WebDriver driver) {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			
			FileUtils.copyFile(scrFile, new File( DynamicQuery.CONFIG_DEFECT_SCREENSHOT+PublicVariables.SEP+currentTestSuite+"\\"+fileName+".jpg"));
		} catch (IOException e) {
			
			e.printStackTrace();
		}	   
	    
	}
}
