package com.infinityfw.libraries;

import org.openqa.selenium.ie.InternetExplorerDriver;

import com.infinityfw.wrappers.SetBaseState;

public class InternetExplorerLauncher extends WebDriverHandler
{
  public void launchWebDriver()
  {
    SetBaseState.driver = new InternetExplorerDriver();
  }

  public void launchWebdriverCustomProfile(String profilePath)
  {
  }
}