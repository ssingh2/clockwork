package com.infinityfw.libraries;

import org.openqa.selenium.chrome.ChromeDriver;

import com.infinityfw.wrappers.SetBaseState;

public class ChromeLauncher extends WebDriverHandler
{
  public void launchWebDriver()
  {
    System.setProperty("webdriver.chrome.driver", DynamicQuery.CONFIG_CHROMEPATH);
    SetBaseState.driver = new ChromeDriver();
  }

  public void launchWebdriverCustomProfile(String profilePath)
  {
  }
}
