package com.infinityfw.libraries;

import org.openqa.selenium.firefox.FirefoxDriver;

import com.infinityfw.wrappers.SetBaseState;

public class FirefoxLauncher extends WebDriverHandler
{
  public void launchWebDriver()
  {
    SetBaseState.driver = new FirefoxDriver();
  }

  public void launchWebdriverCustomProfile(String profilePath)
  {
  }
}