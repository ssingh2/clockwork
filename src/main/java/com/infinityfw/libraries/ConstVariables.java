package com.infinityfw.libraries;



public class ConstVariables
{

  public static final String specialCharacter = "|";
  public static int ACTION_PASSED = 0;

  public static int ACTION_FAILED = 1;

  public static int ACTION_DEFECT = 2;

  public static String SNAPSHOT_EXTENSION = "jpg";

  public static String SNAPSHOT = "Snapshot";


  
  public static final String BUILDSTATUS_NORMAL = "NORMAL";
  public static final String BUILDSTATUS_WARNING = "WARNING";
  public static final String BUILDSTATUS_FAILURE = "FAILURE";
  public static final String BUILDSTATUS_ERROR = "ERROR";
  public static final String ObjectBank = "ObjectBank";
  public static String[] activeKeys={ "MasterFilePath", "xmlPath", "CapturePassBitMap", "CaptureFailBitMap", "CaptureDefectBitMap", 
    "ProjectName","SyncTimeInSeconds", "CI_Tool", "BuildNum", "ReleaseNum", "ChromeDriverPath", 
    "reportPath", "DBType", "Server", "ServerPort", "DBUserName", "DBUserName", "DBPassword","IEDriverPath" };

}