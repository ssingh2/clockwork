
package com.infinityfw.readlxls;

import java.util.Hashtable;

import com.infinityfw.libraries.DynamicQuery;
import com.infinityfw.libraries.PublicVariables;


public class ReadingDataSheetOfTestCasesCopy 
{
    // Function that read all the data from the work sheet corresponding to single test case 
	public static Object[][] getData(String testName,String currentTestSuite , XLReader xls)
	{
		
		xls = new XLReader(DynamicQuery.CONFIG_EXCEL_FILE_PATH+PublicVariables.SEP+currentTestSuite+".xlsx");
		String tmpStrTestSuiteDataSheetName = (PublicVariables.TEST_SUITE_DATA_SHEET_NAME);
		Object data[][];
		
		int counter = 0;
		// Finding the row corresponding to test case
		int testStartRowNum = 1;
		int tmpRowCount=xls.getRowCount(tmpStrTestSuiteDataSheetName);
		// Loop will execute till the cell data is not equal to test name and testStartRowNum is less than number of rows in sheet
		while(!xls.getCellData(tmpStrTestSuiteDataSheetName, 0, testStartRowNum).equals(testName) && testStartRowNum < tmpRowCount)
		{
			// Incrementing testStartRowNum variable as row data for required test name yet not reached
			testStartRowNum++;
			if(xls.getCellData(tmpStrTestSuiteDataSheetName, 0, testStartRowNum).equals(testName))
			{
				// This will be incremented only once when cell with the data same as test name will be retrieved
				counter++;
				break;
			}
		}
		// The value of counter equals to zero when testStartRowNum is not equal to one denotes testcase with given test name does not contain data in data sheet
		if(counter==0 && testStartRowNum!=1)
		{
			data=new Object[1][1];
			Hashtable<String,String> tmpHT=new Hashtable<>();
			tmpHT.put("empty data set", "No data for this test case...!");
			data[0][0]=tmpHT;
			return data;
		}
		
		// Row number for column headings for variable names of test case in data sheet 
		int colHeadStartRowNum = testStartRowNum+1;
		// Logging.log("Test starts from row number - "+testStartRowNum+", for test case -"+testName);
		
		// Row number for data values for variable names of test case in data sheet
		int dataStartRowNum = testStartRowNum+2;
		// Variable to store number of rows for data of particular test case
		int testRowCount = 0;
		// In the excel sheet there are empty rows after single set of data of each test case, here we get total number of rows containing data for that test case
		while(!xls.getCellData(tmpStrTestSuiteDataSheetName, 0,dataStartRowNum+testRowCount).equals(""))
			testRowCount++;
		//Logging.log("Total number of Data rows for test case - "+testName+"= "+testRowCount);
		
		// Finding no. of columns or number of variables in particular test case 
		int testColumnCount = 0;
		while(!xls.getCellData(tmpStrTestSuiteDataSheetName, testColumnCount, testStartRowNum+1).equals(""))
		    testColumnCount++;
		// Logging.log("Total number of Columns for test case - "+testName+"= "+testColumnCount);
		// data array stores objects of HashTable<String,String> and each HashTable<,> object stores single row data useful for single iteration
		int testRowCountYMode=0;
		for(int tmpi=dataStartRowNum;tmpi<dataStartRowNum+testRowCount;tmpi++)
		{
			if(xls.getCellData(tmpStrTestSuiteDataSheetName, 0, tmpi).trim().toLowerCase().equals("y"))
			{
				testRowCountYMode++;
			}		
		}
		if(testRowCountYMode!=0)
		{
		  data = new Object[testRowCountYMode][1];
		  Hashtable<String,String> testDataHashTable = null;
		  int index = 0;
		  // Loop that traverse through those rows of data excel sheet containing variables for test name passed
		  for (int tmpRowNum=dataStartRowNum; tmpRowNum<dataStartRowNum+testRowCount;tmpRowNum++)
		  {
			 // Loop that traverse through all the columns in the current row
			 testDataHashTable = new Hashtable<String,String>();
			 if(xls.getCellData(tmpStrTestSuiteDataSheetName, 0, tmpRowNum).trim().toLowerCase().equals("y"))
			 {		
			   for(int tmpColNum=0;tmpColNum<testColumnCount;tmpColNum++)
			   {
				  // Retrieving column heading for the data column, this column heading is corresponding to variables in test case
				  String key = xls.getCellData(tmpStrTestSuiteDataSheetName, tmpColNum, colHeadStartRowNum);
				  // Retrieving cell data, this  data is equivalent to value for the variables in test case 
				  String value = xls.getCellData(tmpStrTestSuiteDataSheetName, tmpColNum, tmpRowNum);
				  testDataHashTable.put(key, value);
			   }
			  // Storing 
               // Column index is constant as "data" array contains single column
			  data[index][0] =testDataHashTable;
			  index++;
			}
			else
				System.out.println("Skipping the data set...!"+xls.getCellData(tmpStrTestSuiteDataSheetName, 0, tmpRowNum).trim());
		 }
		}
		else
		{
			//All the data set have run mode N 
			data=new Object[1][1];
			Hashtable<String,String> tmpHT=new Hashtable<>();
			tmpHT.put("n data set", "All data set is having run mode N ...!");
			data[0][0]=tmpHT;
		}
		return data;
	}

}
