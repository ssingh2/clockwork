package com.infinityfw.ObjectBank.exceptions;

public class ObjectFinderException extends Exception
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 8278077518409638072L;
String message;

  public ObjectFinderException()
  {
  }

  public ObjectFinderException(String paramString)
  {
    this.message = paramString;
  }

  public String toString()
  {
    return this.message;
  }
}
