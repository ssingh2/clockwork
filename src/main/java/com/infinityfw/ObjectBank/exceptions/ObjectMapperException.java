package com.infinityfw.ObjectBank.exceptions;

public class ObjectMapperException extends Exception
{
  /**
	 * 
	 */
	private static final long serialVersionUID = -2922356276852572916L;
String message;

  public ObjectMapperException()
  {
  }

  public ObjectMapperException(String paramString)
  {
    this.message = paramString;
  }

  public String toString()
  {
    return this.message;
  }
}