package com.infinityfw.ObjectBank.exceptions;

public class ObjectBankException extends Exception
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 718236338283531455L;
String message;

  public ObjectBankException()
  {
  }

  public ObjectBankException(String paramString)
  {
    this.message = paramString;
  }

  public String toString()
  {
    return this.message;
  }
}
