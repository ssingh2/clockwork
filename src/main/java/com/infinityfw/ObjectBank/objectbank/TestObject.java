package com.infinityfw.ObjectBank.objectbank;

public class TestObject
{
  private String objectName;
  private String objectType;

  public String getObjectName()
  {
    return this.objectName;
  }

  public void setObjectName(String paramString)
  {
    this.objectName = paramString;
  }

  public String getObjectType()
  {
    return this.objectType;
  }

  public void setObjectType(String paramString)
  {
    this.objectType = paramString;
  }

  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof TestObject)) && (((TestObject)paramObject).getObjectName().equals(getObjectName()));
  }

  public int hashCode()
  {
    int i = 7;
    i = 71 * i + (this.objectName != null ? this.objectName.hashCode() : 0);
    i = 71 * i + (this.objectType != null ? this.objectType.hashCode() : 0);
    return i;
  }
}
