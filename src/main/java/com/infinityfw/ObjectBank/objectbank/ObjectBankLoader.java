package com.infinityfw.ObjectBank.objectbank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.infinityfw.ObjectBank.exceptions.ObjectBankException;

public class ObjectBankLoader
{
  private String xmlFile;
  private Document document;
  private Map<TestObject, Map<String, String>> objectMap;
  DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

  public ObjectBankLoader(String paramString)
    throws ObjectBankException
  {
    setXmlFile(paramString);
    loadObjectMap();
  }

  private void loadObjectMap()
    throws ObjectBankException
  {
    try
    {
      DocumentBuilder localDocumentBuilder = this.builderFactory.newDocumentBuilder();
      this.document = localDocumentBuilder.parse(new FileInputStream(getXmlFile()));
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      throw new ObjectBankException("The Object Bank is Not properly Structured");
    }
    catch (SAXException localSAXException)
    {
      throw new ObjectBankException("The Object Bank is Not properly Structured");
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      throw new ObjectBankException("The Object Bank File Does not Exist for the Corresponding Script");
    }
    catch (IOException localIOException)
    {
      throw new ObjectBankException("Problem Occured while reading Object Bank");
    }
  }

  public Map<TestObject, Map<String, String>> getObjects()
  {
    Element localElement1 = this.document.getDocumentElement();
    this.objectMap = new LinkedHashMap();
    NodeList localNodeList = localElement1.getElementsByTagName("Object");
    for (int i = 0; i < localNodeList.getLength(); i++)
    {
      Node localNode = localNodeList.item(i);
      Element localElement2 = (Element)localNode;
      this.objectMap.put(getObject(localElement2), getObjectProperties(localElement2));
    }
    return this.objectMap;
  }

  private TestObject getObject(Element paramElement)
  {
    String str1 = paramElement.getAttribute("name");
    String str2 = paramElement.getAttribute("type");
    TestObject localTestObject = new TestObject();
    localTestObject.setObjectName(str1);
    localTestObject.setObjectType(str2);
    return localTestObject;
  }

  private Map<String, String> getObjectProperties(Element paramElement)
  {
    NodeList localNodeList = paramElement.getElementsByTagName("property");
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    for (int i = 0; i < localNodeList.getLength(); i++)
    {
      Node localNode = localNodeList.item(i);
      Element localElement = (Element)localNode;
      String str1 = localElement.getAttribute("name");
      String str2 = localElement.getAttribute("value");
      localLinkedHashMap.put(str1, str2);
    }
    return localLinkedHashMap;
  }

  public String getXmlFile()
  {
    return this.xmlFile;
  }

  private void setXmlFile(String paramString)
  {
    this.xmlFile = paramString;
  }
}
