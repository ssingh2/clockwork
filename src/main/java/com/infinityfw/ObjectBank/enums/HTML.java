package com.infinityfw.ObjectBank.enums;

public enum HTML
{
  AREA("Area Tag", "area"), BUTTON("Input Button", "input"), CAPTION("Table Caption", "caption"), CHECKBOX("Input Check Box", "input"), DIV("Div Tag", "div"), DEFINATION_LIST("Dl : Defination List tag", "dl"), FORM("Form", "form"), FRAME("Frame", "frame"), IMAGE("Image", "img"), INPUT("Input Text Box", "input"), LABEL("Label", "label"), LEGEND("Legend", "legend"), LINK("Link", "a"), LISTITEM("Li : List Item", "li"), MAP("Map Tag", "map"), ORDEREDLIST("Ol : Ordered List Tag", "ol"), OPTION("Option tag inside Select", "option"), PARAGRAPH("P : Paragraph Tag", "p"), PRE("Pre : Preformatted Tag", "pre"), RADIO("Input Radio Button", "input"), SELECT("Select", "select"), TABLE("Table", "table"), TBODY("TBody : Table Body", "tbody"), TD("TD", "td"), TEXTAREA("Input Text Area", "input"), TR("TR", "tr"), TFOOT("TFoot : Table Footer", "tfoot"), UL("UL : Unordered List Tag", "ul");

  private String tagName;
  private String tagDesc;

  public String getTag()
  {
    return this.tagName;
  }

  public String getTagDescription()
  {
    return this.tagDesc;
  }

  private HTML(String paramString1, String paramString2)
  {
    this.tagName = paramString2;
    this.tagDesc = paramString1;
  }
}