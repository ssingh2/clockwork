package com.infinityfw.ObjectBank.enums;

public enum LocatableElements
{
  ID("id"), XPATH("xpath"), NAME("name"), CSS("css"), LINKTEXT("linktext"), PARTIALLINKTEXT("partiallinktext");

  private String locator;

  public String getLocator()
  {
    return this.locator;
  }

  private LocatableElements(String paramString)
  {
    this.locator = paramString;
  }
}