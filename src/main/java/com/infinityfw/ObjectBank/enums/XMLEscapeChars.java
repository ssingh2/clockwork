package com.infinityfw.ObjectBank.enums;

public enum XMLEscapeChars
{
  DOUBLEQUOTES("&quot;", "\"");

  private String xmlEscapeChar;
  private String javaChar;

  public String getXMLEscapeChar()
  {
    return this.xmlEscapeChar;
  }

  public String getJavaChar()
  {
    return this.javaChar;
  }

  private XMLEscapeChars(String paramString1, String paramString2)
  {
    this.xmlEscapeChar = paramString1;
    this.javaChar = paramString2;
  }
}