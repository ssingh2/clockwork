package com.infinityfw.tm;

public class TMFactory {
	
	 private static final String TESTLINK = "TESTLINK";
	  private static final String DEFAULT = "DEFAULT";
	  private static TMHandler m_TMH = null;
	  private static String m_tmToolName = "";

	  public static TMHandler getInstance(String tmToolName)
	  {
	    if ((m_TMH == null) || (!tmToolName.equalsIgnoreCase(m_tmToolName))) {
	    	m_TMH = newInstance(tmToolName);
	    }

	    return m_TMH;
	  }

	  private static TMHandler newInstance(String toolName)
	  {
		  TMHandler tih = null;

	    if (toolName != null) {
	      if (toolName.equalsIgnoreCase(TESTLINK))
	    	  tih = new TestLinkHandler();
	      else {
	    	  tih = new NoTMHandler();
	      }
	    }
	    return tih;
	  }

}
