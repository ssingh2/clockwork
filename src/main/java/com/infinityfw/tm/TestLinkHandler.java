package com.infinityfw.tm;


import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIHelper;
import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.model.Attachment;

import com.infinityfw.libraries.DynamicQuery;

public class TestLinkHandler extends TMHandler{

	public static void reportTestCaseResult(String PROJECT_NAME, String PLAN_NAME, String caseTest, String nameBuild, String msg, String result, Integer bugID) throws TestLinkAPIException {
		TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(DynamicQuery.CONFIG_DEVKEY, DynamicQuery.CONFIG_SERVERURL);
		if(bugID>0){
			Integer projectID = TestLinkAPIHelper.getProjectID (testlinkAPIClient, PROJECT_NAME);  
			Integer testPlanID= TestLinkAPIHelper.getPlanID (testlinkAPIClient, projectID, PLAN_NAME);  
			Integer testCaseID= TestLinkAPIHelper.getTestCaseID (testlinkAPIClient, projectID, caseTest);  
			Integer BUILDID =TestLinkAPIHelper.getBuildID (testlinkAPIClient, testPlanID, nameBuild);  
			testlinkAPIClient.reportTestCaseResult (testPlanID, testCaseID, BUILDID, bugID, false, msg, result); 					
		}else{
			testlinkAPIClient.reportTestCaseResult(PROJECT_NAME, PLAN_NAME, caseTest, nameBuild, msg, result);
		}
	}
	
	@ Deprecated
	public static void updateTestLinkResult(String PROJECT_NAME, String PLAN_NAME, String caseTest, String nameBuild, String msg, String result )   throws TestLinkAPIException {
		TestLinkAPI api = null;
		TestLinkAPIClient testlinkAPIClient =  new TestLinkAPIClient(DynamicQuery.CONFIG_DEVKEY, DynamicQuery.CONFIG_SERVERURL);
		new TestLinkAPIHelper();
		File attachmentFile = new File("d:\\temp\\selenium_temp.jpg");
		String fileContent = null;
		try {
			byte[] byteArray = FileUtils.readFileToByteArray(attachmentFile);
			fileContent = new String(Base64.encodeBase64(byteArray));
		} catch (IOException e) {
			e.printStackTrace( System.err );
			System.exit(-1);
		}
		
		  Attachment attachment =  api.uploadExecutionAttachment(
		            1, //executionId 
		            "Setting customer plan", //title 
		            "In this screen the attendant is defining the customer plan", //description  
		            "screenshot_customer_plan_"+System.currentTimeMillis()+".jpg", //fileName 
		            "image/jpeg", //fileType
		            fileContent); //content
		testlinkAPIClient.reportTestCaseResult(PROJECT_NAME, PLAN_NAME,caseTest, nameBuild, msg, result);         
	}


}
